#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAX 250


int main(int argc, char **argv){
	char buff[MAX];
	float *arreglo;
	arreglo=(float *)calloc(MAX,sizeof(float));
	int index=0;
	float suma=0;
	float promedio=0;
	float mayor=0;
	float menor=0;
	float difMedia=0;
	float parcialCuadrado=0;
	float varianza=0;
	float desviacion=0;
	if(arreglo==NULL){
		return -1;
	}

	while(1){
		fgets(buff,MAX,stdin);
		if(strcmp(buff,"x\n")==0){
			break;
		}
		else{
			*(arreglo+index)=atoi(buff);
			suma=suma+(*(arreglo+index));
			index ++;
		}
	}

	menor=*(arreglo+0);
	promedio=suma/index;

	for(int j=0;j<index;j++){
		if(mayor<*(arreglo+j)){
			mayor=*(arreglo+j);
		}

		if(menor>*(arreglo+j)){
			menor=*(arreglo+j);
		}

		difMedia=((*(arreglo+j))-promedio);
		parcialCuadrado=parcialCuadrado+pow(difMedia,2);
	}
	varianza=parcialCuadrado/index;
	desviacion=sqrt(varianza);

	printf("max\tsuma\tmin\tdesv\n%.2f\t%.2f\t%.2f\t%.2f\n",mayor,suma,menor,desviacion);
	free(arreglo);
	return 0;

}